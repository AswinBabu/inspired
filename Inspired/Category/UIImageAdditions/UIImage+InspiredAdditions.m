//
//  UIImage+InspiredAdditions.m
//  Inspired
//
//  Created by Aswin on 04/10/16.
//  Copyright © 2016 Aswin. All rights reserved.
//

#import "UIImage+InspiredAdditions.h"


@implementation UIImage (InspiredAdditions)

#pragma mark - SaveImage 
- (BOOL)saveImageToDirectoryWithName:(NSString *)imageName {
    NSData *imageData = UIImagePNGRepresentation(self);
    NSString *imageFilePath = [self documentsPathForFileName:imageName];
    
//    NSLog(@"%@",imageFilePath);
    return [imageData writeToFile:imageFilePath atomically:YES]; //Write the file
}


- (NSString *)documentsPathForFileName:(NSString *)name {
    NSString *documentsPath = [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory,NSUserDomainMask, YES) objectAtIndex:0];
    return [documentsPath stringByAppendingPathComponent:name];
}



@end
