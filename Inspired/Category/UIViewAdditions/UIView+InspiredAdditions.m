//
//  UIView+InspiredAdditions.m
//  Inspired
//
//  Created by Aswin on 04/10/16.
//  Copyright © 2016 Aswin. All rights reserved.
//

#import "UIView+InspiredAdditions.h"

@implementation UIView (InspiredAdditions)

#pragma mark - SnapShot
- (UIImage *)takeSnapShotWithSize:(CGSize)printSize {
    
    UIGraphicsBeginImageContextWithOptions(printSize, NO, [UIScreen mainScreen].scale);
//    CGContextSetInterpolationQuality(UIGraphicsGetCurrentContext(), kCGInterpolationHigh);
    [self drawViewHierarchyInRect:CGRectMake(0, 0, printSize.width, printSize.height) afterScreenUpdates:YES];
    UIImage *image = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    return image;
}



@end
