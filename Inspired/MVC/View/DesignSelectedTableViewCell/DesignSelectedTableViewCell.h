//
//  DesignSelectedTableViewCell.h
//  Inspired
//
//  Created by Siju Karunakaran on 13/10/16.
//  Copyright © 2016 Aswin. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "DropView.h"

@protocol DesignSelectedCellDelegate <NSObject>
- (void)removeDropView:(DropView *)view;
@end

@interface DesignSelectedTableViewCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UIImageView *imageViewDesign;
@property (weak, nonatomic) IBOutlet UILabel *labelName;
@property(nonatomic, strong) id<DesignSelectedCellDelegate> delegate;
@property(nonatomic, strong) DropView *viewDropped;

@end
