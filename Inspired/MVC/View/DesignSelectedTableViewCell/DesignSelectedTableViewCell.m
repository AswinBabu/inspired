//
//  DesignSelectedTableViewCell.m
//  Inspired
//
//  Created by Siju Karunakaran on 13/10/16.
//  Copyright © 2016 Aswin. All rights reserved.
//

#import "DesignSelectedTableViewCell.h"

@implementation DesignSelectedTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];
}


#pragma mark - ButtonAction
- (IBAction)closeButtonAction:(id)sender {
    [_delegate removeDropView:_viewDropped];
}


@end
