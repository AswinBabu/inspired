//
//  DesignCollectionViewCell.h
//  Inspired
//
//  Created by Aswin on 27/09/16.
//  Copyright © 2016 Aswin. All rights reserved.
//

#import <UIKit/UIKit.h>

@class DesignCollectionViewCell;

@protocol DesignCollectionViewCellDelegate <NSObject>
- (void)didLongPressCell:(DesignCollectionViewCell *)cell withGesture:(UILongPressGestureRecognizer *)gesture;
@end

@interface DesignCollectionViewCell : UICollectionViewCell

@property(nonatomic, strong) id<DesignCollectionViewCellDelegate> delegate;

- (void)addImage:(UIImage *)imageDesign;

@end
