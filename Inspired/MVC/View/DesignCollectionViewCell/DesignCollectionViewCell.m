//
//  DesignCollectionViewCell.m
//  Inspired
//
//  Created by Aswin on 27/09/16.
//  Copyright © 2016 Aswin. All rights reserved.
//

#import "DesignCollectionViewCell.h"

@implementation DesignCollectionViewCell {
    
    __weak IBOutlet UIImageView *imageViewDesign;
}

- (void)awakeFromNib {
    [super awakeFromNib];
    
    self.contentView.layer.borderWidth = 0.5;
    self.contentView.layer.borderColor = [UIColor lightGrayColor].CGColor;
    self.contentView.layer.masksToBounds = YES;
    [self addLongPressGensture];
}

- (void)prepareForReuse {
    [super prepareForReuse];
    [imageViewDesign setImage:nil];
}

#pragma mark - LoadContent
- (void)addImage:(UIImage *)imageDesign {
    [imageViewDesign setImage:imageDesign];
}

#pragma mark - AddGesture
- (void)addLongPressGensture {
    UILongPressGestureRecognizer *longPressGesture = [[UILongPressGestureRecognizer alloc] initWithTarget:self action:@selector(handleLongPress:)];
    longPressGesture.numberOfTouchesRequired = 1;
    longPressGesture.minimumPressDuration    = 0.1f;
    [imageViewDesign addGestureRecognizer:longPressGesture];
}

#pragma mark - GestureRecognizer
- (void)handleLongPress:(UILongPressGestureRecognizer *)gesture {
    if (gesture.state == UIGestureRecognizerStateBegan) {
        [_delegate didLongPressCell:self withGesture:gesture];
    }
    else if (gesture.state == UIGestureRecognizerStateCancelled || gesture.state == UIGestureRecognizerStateFailed || gesture.state == UIGestureRecognizerStateEnded) {
        [_delegate didLongPressCell:self withGesture:gesture];
    }
}

@end
