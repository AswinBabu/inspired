//
//  DragView.h
//  Inspired
//
//  Created by Aswin on 29/09/16.
//  Copyright © 2016 Aswin. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface DragView : UIView

@property(nonatomic, strong) UIImageView *imageViewDesign;
@property(nonatomic, strong) UIImage *imageDesign;

- (instancetype)initWithDesign:(UIImage *)imageDesign;


@end
