//
//  DragView.m
//  Inspired
//
//  Created by Aswin on 29/09/16.
//  Copyright © 2016 Aswin. All rights reserved.
//

#import "DragView.h"

@implementation DragView

- (instancetype)initWithDesign:(UIImage *)imageDesign {
    self = [super init];
    if (self) {
        self.frame = CGRectMake(0.0, 0.0, imageDesign.size.width * 0.05, imageDesign.size.height * 0.05);
        [self setBackgroundColor:[UIColor clearColor]];
        _imageDesign = imageDesign;
        [self addImageToView:imageDesign];
    }
    return self;
}

#pragma mark - AddImage
- (void)addImageToView:(UIImage *)image {
    _imageViewDesign = [[UIImageView alloc] initWithFrame:self.bounds];
    [_imageViewDesign setContentMode:UIViewContentModeScaleAspectFit];
    [_imageViewDesign setImage:image];
    [_imageViewDesign setBackgroundColor:[UIColor clearColor]];
    [self addSubview:_imageViewDesign];
}

@end
