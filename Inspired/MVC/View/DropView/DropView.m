//
//  DroppedView.m
//  Inspired
//
//  Created by Aswin on 29/09/16.
//  Copyright © 2016 Aswin. All rights reserved.
//

#import "DropView.h"

#define kPinchScaleMax 5.0
#define kPinchScaleMin 0.20
@implementation DropView {
    UIButton * buttonClose;
    
    CGFloat currentPinchScale;
}
@synthesize selected = _selected;

- (instancetype)initWithDesign:(UIImage *)imageDesign {
    self = [super initWithDesign:imageDesign];
    if (self) {
//        [self addGestureRecognizers];
        [self setUI];
    }
    return self;
}

#pragma mark - SetUI
- (void)setUI {
    buttonClose = [UIButton buttonWithType:UIButtonTypeCustom];
    [buttonClose setFrame:CGRectMake(0, 0, 20, 20)];
    [buttonClose addTarget:self action:@selector(closeButtonAction:) forControlEvents:UIControlEventTouchUpInside];
    [buttonClose setBackgroundImage:[UIImage imageNamed:@"close"] forState:UIControlStateNormal];
    buttonClose.center = CGPointMake(8.0, 8.0);
    [self addSubview:buttonClose];
    [buttonClose setHidden:YES];
}

#pragma mark - Getter/Setter
- (void)setSelected:(BOOL)selected{
    _selected = selected;
//    [self enableBorder:selected];
    [self setBackgroundColor:_selected?[UIColor colorWithRed:0.873 green:0.868 blue:0.877 alpha:1.00]:[UIColor clearColor]];
    if (selected == NO) {
        [buttonClose setHidden:YES];
    }
}

- (BOOL)isSelected {
    return _selected;
}

#pragma mark - PublicMethods
- (void)changeDesignWithColor:(UIColor *)color {
    _colorDesign = color;
    self.imageDesign = [self.imageDesign imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
    self.imageViewDesign.image = self.imageDesign;
    [self.imageViewDesign setTintColor:color];
    [_delegate didChangeColorOfDropView:self];
}

#pragma mark - PrivateMethods
#pragma mark AddBorder
- (void)addBorder {
    self.layer.borderColor = [UIColor lightGrayColor].CGColor;
    self.layer.masksToBounds = YES;
}

- (void)enableBorder:(BOOL)enableBorder {
    self.layer.borderWidth = enableBorder ? 0.5 : 0.0;
}

- (void)selectView {
    [self setSelected:YES];
    [_delegate didSelectDropView:self];
}

/*
#pragma mark AddGesture
- (void)addGestureRecognizers{
    UITapGestureRecognizer *tapGesture = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(handleTap:)];
    [self addGestureRecognizer:tapGesture];
    
    UIPanGestureRecognizer *panRecognizer = [[UIPanGestureRecognizer alloc] initWithTarget:self action:@selector(handleMove:)];
    [panRecognizer setMinimumNumberOfTouches:1];
    [panRecognizer setMaximumNumberOfTouches:1];
    [self addGestureRecognizer:panRecognizer];
    
    UIRotationGestureRecognizer *rotateRecognizer = [[UIRotationGestureRecognizer alloc] initWithTarget:self action:@selector(handleRotate:)];
    self.multipleTouchEnabled = YES;
    [self addGestureRecognizer:rotateRecognizer];
    
    UIPinchGestureRecognizer *pinchRecognizer = [[UIPinchGestureRecognizer alloc] initWithTarget:self action:@selector(handlePinch:)];
    [self addGestureRecognizer:pinchRecognizer];
    
    UILongPressGestureRecognizer *longPressRecognizer = [[UILongPressGestureRecognizer alloc] initWithTarget:self action:@selector(handleLongPress:)];
    longPressRecognizer.minimumPressDuration = 0.8;
    [self addGestureRecognizer:longPressRecognizer];
}

#pragma mark GestureRecognizer
- (void)handleTap:(UITapGestureRecognizer *)gesture {
    [self selectView];
}

- (void)handleMove:(UIPanGestureRecognizer *)recognizer {
    [self selectView];
    CGPoint translation = [recognizer translationInView:self.superview];
    recognizer.view.center = CGPointMake(recognizer.view.center.x+translation.x, recognizer.view.center.y+translation.y);
    [recognizer setTranslation:CGPointMake(0, 0) inView:self.superview];
}

- (void)handleRotate:(UIRotationGestureRecognizer *)recognizer {
    [self selectView];
    recognizer.view.transform = CGAffineTransformRotate(recognizer.view.transform, recognizer.rotation);
    recognizer.rotation = 0;
}

- (void)handlePinch:(UIPinchGestureRecognizer *)recognizer {
    [self selectView];
    if(recognizer.state == UIGestureRecognizerStateBegan) {
        currentPinchScale = recognizer.scale;
    }
    
    if (recognizer.state == UIGestureRecognizerStateBegan || recognizer.state == UIGestureRecognizerStateChanged) {
        
        CGFloat currentScale = [[recognizer.view.layer valueForKeyPath:@"transform.scale"] floatValue];
        CGFloat newScale = 1 -  (currentPinchScale - recognizer.scale);
        newScale = MIN(newScale, kPinchScaleMax / currentScale);
        newScale = MAX(newScale, kPinchScaleMin / currentScale);
        CGAffineTransform transform = CGAffineTransformScale(recognizer.view.transform, newScale, newScale);
        recognizer.view.transform = transform;
        currentPinchScale = recognizer.scale;
    }
}

- (void)handleLongPress:(UILongPressGestureRecognizer *)recognizer {
    [self selectView];
    [buttonClose setHidden:NO];
}
*/
#pragma mark - ButtonAction
- (void)closeButtonAction:(id)sender {
    [_delegate didRemoveDropView:self];
    [self removeFromSuperview];
}

@end
