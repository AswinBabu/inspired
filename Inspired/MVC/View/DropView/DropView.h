//
//  DroppedView.h
//  Inspired
//
//  Created by Aswin on 29/09/16.
//  Copyright © 2016 Aswin. All rights reserved.
//

#import "DragView.h"

@class DropView;

@protocol DropViewDelegate <NSObject>
- (void)didSelectDropView:(DropView *)view;
@optional
- (void)didRemoveDropView:(DropView *)view;
- (void)didChangeColorOfDropView:(DropView *)view;
@end

@interface DropView : DragView

@property(nonatomic, assign, getter=isSelected) BOOL selected;
@property(nonatomic, strong) id<DropViewDelegate> delegate;
@property(nonatomic, strong) UIColor *colorDesign;

- (void)changeDesignWithColor:(UIColor *)color;

@end
