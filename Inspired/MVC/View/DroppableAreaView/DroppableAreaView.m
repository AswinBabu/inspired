//
//  DroppableAreaView.m
//  Inspired
//
//  Created by Aswin on 14/10/16.
//  Copyright © 2016 Aswin. All rights reserved.
//

#import "DroppableAreaView.h"
#import "DropView.h"


#define kPinchScaleMax 5.0
#define kPinchScaleMin 0.20

@implementation DroppableAreaView {
    
    CGFloat currentPinchScale;
    
//    CAShapeLayer *borderLayer;
}

- (void)awakeFromNib {
    [super awakeFromNib];
    
//    [self addDashedBorderLimit];
    [self addGestureRecognizers];
}
/*
#pragma mark - PublicMethods
- (void)addDashedBorderLimit {
    [self layoutIfNeeded];
    borderLayer = [CAShapeLayer layer];
    borderLayer.strokeColor = [UIColor colorWithRed:67/255.0f green:37/255.0f blue:83/255.0f alpha:1].CGColor;
    borderLayer.fillColor = nil;
    borderLayer.lineDashPattern = @[@4, @2];
    [self.layer addSublayer:borderLayer];
    borderLayer.path = [UIBezierPath bezierPathWithRect:self.bounds].CGPath;
    borderLayer.frame = self.bounds;
}

- (void)removeCanvasDashedBorderLimit {
    [_viewBorder removeFromSuperview];
}
*/

#pragma mark - PrivateMethods
#pragma mark GetSeletedSubView
- (DropView *)getSelectedDropView {
    return [self.subviews filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"isSelected == 1"]].firstObject;
}


#pragma mark AddGesture
- (void)addGestureRecognizers{
    UITapGestureRecognizer *tapGesture = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(handleTap:)];
    [self addGestureRecognizer:tapGesture];
    
    UIPanGestureRecognizer *panRecognizer = [[UIPanGestureRecognizer alloc] initWithTarget:self action:@selector(handleMove:)];
    [panRecognizer setMinimumNumberOfTouches:1];
    [panRecognizer setMaximumNumberOfTouches:1];
    [self addGestureRecognizer:panRecognizer];
    
    UIRotationGestureRecognizer *rotateRecognizer = [[UIRotationGestureRecognizer alloc] initWithTarget:self action:@selector(handleRotate:)];
    self.multipleTouchEnabled = YES;
    [self addGestureRecognizer:rotateRecognizer];
    
    UIPinchGestureRecognizer *pinchRecognizer = [[UIPinchGestureRecognizer alloc] initWithTarget:self action:@selector(handlePinch:)];
    [self addGestureRecognizer:pinchRecognizer];
    
    UILongPressGestureRecognizer *longPressRecognizer = [[UILongPressGestureRecognizer alloc] initWithTarget:self action:@selector(handleLongPress:)];
    longPressRecognizer.minimumPressDuration = 0.8;
    [self addGestureRecognizer:longPressRecognizer];
}

#pragma mark GestureRecognizer
- (void)handleTap:(UITapGestureRecognizer *)gesture {
}

- (void)handleMove:(UIPanGestureRecognizer *)recognizer {
    CGPoint translation = [recognizer translationInView:self];
    
    
    DropView *selectedDropView = [self getSelectedDropView];
    
    selectedDropView.center = CGPointMake(selectedDropView.center.x+translation.x, selectedDropView.center.y+translation.y);
    [recognizer setTranslation:CGPointMake(0, 0) inView:self];
}

- (void)handleRotate:(UIRotationGestureRecognizer *)recognizer {
    DropView *selectedDropView = [self getSelectedDropView];
    selectedDropView.transform = CGAffineTransformRotate(selectedDropView.transform, recognizer.rotation);
    recognizer.rotation = 0;
}

- (void)handlePinch:(UIPinchGestureRecognizer *)recognizer {
    if(recognizer.state == UIGestureRecognizerStateBegan) {
        currentPinchScale = recognizer.scale;
    }
    
    if (recognizer.state == UIGestureRecognizerStateBegan || recognizer.state == UIGestureRecognizerStateChanged) {
        DropView *selectedDropView = [self getSelectedDropView];
        CGFloat currentScale = [[selectedDropView.layer valueForKeyPath:@"transform.scale"] floatValue];
        CGFloat newScale = 1 -  (currentPinchScale - recognizer.scale);
        newScale = MIN(newScale, kPinchScaleMax / currentScale);
        newScale = MAX(newScale, kPinchScaleMin / currentScale);
        CGAffineTransform transform = CGAffineTransformScale(selectedDropView.transform, newScale, newScale);
        selectedDropView.transform = transform;
        currentPinchScale = recognizer.scale;
    }
}

- (void)handleLongPress:(UILongPressGestureRecognizer *)recognizer {
}

@end
