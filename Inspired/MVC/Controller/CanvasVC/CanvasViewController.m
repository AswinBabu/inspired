//
//  CanvasViewController.m
//  Inspired
//
//  Created by Aswin on 24/09/16.
//  Copyright © 2016 Aswin. All rights reserved.
//

#import "CanvasViewController.h"
#import "UIImage+InspiredAdditions.h"
#import "UIView+InspiredAdditions.h"
#import "PrintPreviewViewController.h"
#import "DroppableAreaView.h"
#import "DropView.h"
#import "ISColorWheel.h"
#import "DesignSelectedTableViewCell.h"


@interface CanvasViewController ()<UIGestureRecognizerDelegate, DropViewDelegate, UITableViewDataSource, UITableViewDelegate, DesignSelectedCellDelegate, ISColorWheelDelegate>
@end

@implementation CanvasViewController {
    
    __weak IBOutlet UITableView *tableViewDesignList;
    __weak IBOutlet UIImageView *imageViewShirtDesign;
    __weak IBOutlet UIScrollView *scrollViewCanvas;
    __weak IBOutlet UIView *contentView;
    __weak IBOutlet UIView *viewBorder;
    __weak IBOutlet DroppableAreaView *viewDroppableArea;
    __weak IBOutlet UILabel *labelSelectShirtSide;
    __weak IBOutlet NSLayoutConstraint *constraintDroppableViewCentreX;
    __weak IBOutlet ISColorWheel *controlColorWheel;
    
    
    DragView *selectedDragView;
    DropView *selectedDroppedView;
    
    NSMutableArray *arraySubViewDesign;
    
    BOOL enablePanGesture;
    BOOL selectShirtFrontSide;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    enablePanGesture = NO;
    selectShirtFrontSide = NO;
    
    arraySubViewDesign = [NSMutableArray new];
    
    controlColorWheel.delegate = self;
    controlColorWheel.continuous = true;
    
    [self addPanGensture];
    [self addLongPressGesture];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Navigation
#pragma mark Segue
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    if ([[segue identifier] isEqualToString:@"seguePrintPreview"]) {
        [self removeCanvasDashedBorderLimit];
        [viewDroppableArea.subviews makeObjectsPerformSelector:NSSelectorFromString(@"setSelected:") withObject:nil];
        [self.view layoutIfNeeded];
        UIImage *imageTshirtPrintDesign = [contentView takeSnapShotWithSize:contentView.bounds.size];
        UIImage *imagePrintDesign = [viewDroppableArea takeSnapShotWithSize:CGSizeMake(612 , 792)];//(2480, 3508)];
        [imageTshirtPrintDesign saveImageToDirectoryWithName:@"Design.png"];
        [self saveImageToPhotoAlbum:imagePrintDesign];
        [imagePrintDesign saveImageToDirectoryWithName:@"Print.png"];
        [segue.destinationViewController setValue:imageTshirtPrintDesign forKey:@"imagePreview"];
    }
}

#pragma mark - ViewController - DetectTouch
- (void)touchesBegan:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event {
    UITouch *touch ;
    touch = [[event allTouches] anyObject];

    if (![[touch view] isKindOfClass:[DropView class]] ) {
        [self selectView:nil selected:NO andBringToFront:NO];
    }
    
    [super touchesBegan:touches withEvent:event];
}

#pragma mark - PublicMethods

- (void)removeCanvasDashedBorderLimit {
    [viewBorder removeFromSuperview];
}

#pragma mark AddDraggableView
- (void)addDraggableViewWithDesign:(UIImage *)imageSelectedDesign atPoint:(CGPoint)center {
    enablePanGesture = YES;
    selectedDragView = [[DragView alloc] initWithDesign:imageSelectedDesign];
    [self.view addSubview:selectedDragView];
    selectedDragView.center = center;
}

#pragma mark ValidateDropPoint
- (BOOL)isValidDropPoint:(CGPoint)dropPoint {
    [self.view layoutIfNeeded];
    CGRect frame = [self.view convertRect:viewDroppableArea.frame fromView:scrollViewCanvas];
    return CGRectContainsPoint(frame, dropPoint);
}

#pragma mark CancelActivity
- (void)cancelDragAndDropActivity {
    [selectedDragView removeFromSuperview];
    selectedDragView = nil;
    enablePanGesture = NO;
}

#pragma mark - PrivateMethods

- (void)selectView:(DropView *)view selected:(BOOL)selected andBringToFront:(BOOL)bringSubviewToFront {
    [viewDroppableArea.subviews makeObjectsPerformSelector:NSSelectorFromString(@"setSelected:") withObject:nil];
    selectedDroppedView =  selected ? view : nil;
    [selectedDroppedView setSelected:selected];
    [self updateSelectedDesignTableView];
    if (bringSubviewToFront == YES) {
        [viewDroppableArea bringSubviewToFront:selectedDroppedView];
    }
}

#pragma mark AddGesture
- (void)addPanGensture {
    UIPanGestureRecognizer *panGesture = [[UIPanGestureRecognizer alloc] initWithTarget:self action:@selector(handlePan:)];
    panGesture.delegate = self;
    panGesture.cancelsTouchesInView = NO;
    [self.view addGestureRecognizer:panGesture];
}

- (void)addLongPressGesture {
    UILongPressGestureRecognizer *longPressRecognizer = [[UILongPressGestureRecognizer alloc] initWithTarget:self action:@selector(handleLongPress:)];
    longPressRecognizer.minimumPressDuration = 1.0;
    [contentView addGestureRecognizer:longPressRecognizer];
}

#pragma mark GestureRecognizer
- (void)handlePan:(UIPanGestureRecognizer *)gesture {
    if (enablePanGesture) {
        CGPoint touchPoint = [gesture locationInView:self.view];
        if (gesture.state == UIGestureRecognizerStateChanged && [selectedDragView isDescendantOfView:self.view]) {
            selectedDragView.center = touchPoint;
        } else if (gesture.state == UIGestureRecognizerStateRecognized && [selectedDragView isDescendantOfView:self.view]) {
            BOOL validDropPoint = [self isValidDropPoint:touchPoint];
            if (validDropPoint) {
                CGPoint newCenter = [gesture locationInView:viewDroppableArea];
                DropView *viewDropDesign = [[DropView alloc] initWithDesign:selectedDragView.imageDesign];
                viewDropDesign.delegate = self;
                [viewDroppableArea addSubview:viewDropDesign];
                [viewDropDesign setCenter:newCenter];
                [self selectView:viewDropDesign selected:YES andBringToFront:YES];
                [self showSelectedDesignTableView];
                [self updateSelectedDesignTableView];
            }
            [self cancelDragAndDropActivity];
        }
    }
}

- (void)handleLongPress:(UILongPressGestureRecognizer *)recognizer {
    [self showSelectedDesignTableView];
}

#pragma mark PhotoAlbumResponse

- (void)saveImageToPhotoAlbum:(UIImage *)imagePrintDesign {
    UIImageWriteToSavedPhotosAlbum(imagePrintDesign, self, @selector(image:didFinishSavingWithError:contextInfo:), nil);
}

- (void) image:(UIImage*)image didFinishSavingWithError:(NSError *)error contextInfo:(NSDictionary*)info {
    
    UIAlertController * alert=   [UIAlertController alertControllerWithTitle:error == nil ? @"Success" : @"Error" message:error == nil ? @"New design saved to photo album" : @"Failed to save design to photo album" preferredStyle:UIAlertControllerStyleAlert];
    
    UIAlertAction* ok = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction * action) {
        [alert dismissViewControllerAnimated:YES completion:nil];
    }];
    
    [alert addAction:ok];
    
    [self.view.window.rootViewController presentViewController:alert animated:YES completion:nil];
}


#pragma mark - UIGestureRecognizerDelegate
- (BOOL)gestureRecognizer:(UIGestureRecognizer *)gestureRecognizer shouldRecognizeSimultaneouslyWithGestureRecognizer:(UIGestureRecognizer *)otherGestureRecognizer {
    return YES;
}

#pragma mark - DropViewDelegate
- (void)didSelectDropView:(DropView *)view {
    [self selectView:view selected:YES andBringToFront:NO];
}

- (void)didChangeColorOfDropView:(DropView *)view {
    [self updateSelectedDesignTableView];
}

- (void)didRemoveDropView:(DropView *)view {
    [self performSelector:@selector(updateSelectedDesignTableView) withObject:nil afterDelay:0.2];
}

#pragma mark - DesignSelectedCellDelegate
- (void)removeDropView:(DropView *)view {
    [view removeFromSuperview];
    [self updateSelectedDesignTableView];
    [self selectView:arraySubViewDesign.lastObject selected:YES andBringToFront:NO];
    
}

#pragma mark - ISColorWheelDelegate
- (void)colorWheelDidChangeColor:(ISColorWheel *)colorWheel
{
    if (selectedDroppedView != nil) {
        [selectedDroppedView changeDesignWithColor:colorWheel.currentColor];
    }
}

#pragma mark - ButtonAction
- (IBAction)pickColorButtonAction:(id)sender {
    if (selectedDroppedView != nil) {
        [selectedDroppedView changeDesignWithColor:((UIButton *)sender).backgroundColor];
    }
}

- (IBAction)colorBrightnessChangeSliderAction:(id)sender {
    [controlColorWheel setBrightness:((UISlider *)sender).value];
    [self colorWheelDidChangeColor:controlColorWheel];
}

- (IBAction)handleTapSwitchShirtSide:(id)sender {
    
    selectShirtFrontSide = !selectShirtFrontSide;
    constraintDroppableViewCentreX.constant = selectShirtFrontSide ? -8.0 : -13.0;
    [((UIImageView *)((UIGestureRecognizer *)sender).view) setImage:[UIImage imageNamed :selectShirtFrontSide ? @"shirt_Front" : @"shirtDesign_back"]];
    [imageViewShirtDesign setImage:[UIImage imageNamed:selectShirtFrontSide ? @"shirtDesign_back" : @"shirt_Front"]];
    [labelSelectShirtSide setText: selectShirtFrontSide ? @"FRONT SIDE" :@"BACK SIDE"];
}

- (IBAction)handleContentViewTapRemoveSelectedDesign:(id)sender {
    [self selectView:nil selected:NO andBringToFront:NO];
}

- (IBAction)backButtonAction:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
}

- (IBAction)resetButtonAction:(id)sender {
    [viewDroppableArea.subviews makeObjectsPerformSelector:@selector(removeFromSuperview)];
}

#pragma mark - TableView
- (void)hideSelectedDesignTableView {
    if (tableViewDesignList.alpha == 1.0) {
        [UIView animateWithDuration:0.5 delay:0.0 options:UIViewAnimationOptionTransitionCrossDissolve animations:^{
            [tableViewDesignList setAlpha:0.0];
            [tableViewDesignList setHidden:YES];
        } completion:nil];
    }
}

- (void)showSelectedDesignTableView {
    if (tableViewDesignList.alpha == 0.0 && viewDroppableArea.subviews.count > 0) {
        [UIView animateWithDuration:0.5 delay:0.0 options:UIViewAnimationOptionTransitionCrossDissolve animations:^{
            [tableViewDesignList setAlpha:1.0];
            [tableViewDesignList setHidden:NO];
        } completion:^(BOOL finished) {
            [self updateSelectedDesignTableView];
            [tableViewDesignList setEditing:YES animated:YES];
        }];
    }
    else {
        [self updateSelectedDesignTableView];
    }
}

- (void)updateSelectedDesignTableView {
    [arraySubViewDesign removeAllObjects];
    [arraySubViewDesign addObjectsFromArray: viewDroppableArea.subviews];
    
    if (arraySubViewDesign.count == 0) {
        [self hideSelectedDesignTableView];
    }
    [tableViewDesignList reloadData];
}

#pragma mark TableView DataSource
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return arraySubViewDesign.count;
}

- (void)tableView:(UITableView *)tableView willDisplayHeaderView:(UIView *)view forSection:(NSInteger)section {
    UITableViewHeaderFooterView *header = (UITableViewHeaderFooterView *)view;
    [header.textLabel setTextColor:[UIColor colorWithRed:215.0/255.0 green:25.0/255.0 blue:32.0/255.0 alpha:1.0]];
    [header.textLabel setFont:[UIFont fontWithName:@"Avenir-Book" size:15.0]];
}

- (NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section {
    return @"SELECTED DESIGNS";
}

- (UIView *)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section{
    return [UIView new];
}

- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section{
    return 1.0f;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    DesignSelectedTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"SelectedDesignCellId" forIndexPath:indexPath];
    [cell setDelegate:self];
    [cell setBackgroundView:nil];
    cell.imageViewDesign.image = [arraySubViewDesign[indexPath.row] imageDesign];
    cell.imageViewDesign.tintColor = [[arraySubViewDesign[indexPath.row] imageViewDesign] tintColor];
    cell.labelName.text = [NSString stringWithFormat:@"Layer %02d", indexPath.row + 1];
    
    [cell setBackgroundColor:arraySubViewDesign[indexPath.row] == selectedDroppedView ? [UIColor colorWithRed:0.873 green:0.868 blue:0.877 alpha:1.00] : [UIColor clearColor]];
    cell.viewDropped = arraySubViewDesign[indexPath.row];

    return cell;
}

- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath {
    return YES;
}

- (UITableViewCellEditingStyle)tableView:(UITableView *)tableView editingStyleForRowAtIndexPath:(NSIndexPath *)indexPath {
    return UITableViewCellEditingStyleNone;
}

- (BOOL)tableView:(UITableView *)tableview shouldIndentWhileEditingRowAtIndexPath:(NSIndexPath *)indexPath {
    return NO;
}

- (BOOL)tableView:(UITableView *)tableView canMoveRowAtIndexPath:(NSIndexPath *)indexPath {
    return YES;
}

- (void)tableView:(UITableView *)tableView moveRowAtIndexPath:(NSIndexPath *)sourceIndexPath toIndexPath:(NSIndexPath *)destinationIndexPath {
    DropView *moveView = arraySubViewDesign[sourceIndexPath.row];
    [arraySubViewDesign removeObjectAtIndex:sourceIndexPath.row];
    [arraySubViewDesign insertObject:moveView atIndex:destinationIndexPath.row];
    
    if (destinationIndexPath.row < sourceIndexPath.row) {
        [moveView removeFromSuperview];
    }
    [viewDroppableArea insertSubview:moveView atIndex:destinationIndexPath.row];
}

#pragma mark TableView Delegate
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    
    [self selectView:arraySubViewDesign[indexPath.row] selected:!(arraySubViewDesign[indexPath.row] == selectedDroppedView) andBringToFront:NO];
}


@end
