//
//  DesignCollectionViewController.m
//  Inspired
//
//  Created by Aswin on 28/09/16.
//  Copyright © 2016 Aswin. All rights reserved.
//

#import "DesignCollectionViewController.h"
#import "DesignCollectionViewCell.h"
#import "CanvasViewController.h"
@interface DesignCollectionViewController ()<DesignCollectionViewCellDelegate>

@end

@implementation DesignCollectionViewController {
    NSArray *arrayDesigns;
}

static NSString * const reuseIdentifier = @"Cell";

- (void)viewDidLoad {
    [super viewDidLoad];
    
    arrayDesigns = [self getDesigns];
    [self.collectionView reloadData];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - LoadContent

- (NSArray *)getDesigns {
    NSMutableArray *arrayAsset = [NSMutableArray new];
    for (int i = 0; i < 43; i++) {
        [arrayAsset addObject:[UIImage imageNamed:[NSString stringWithFormat:@"Design%d",i]]];
    }
    return [arrayAsset copy];
}


#pragma mark - UICollectionView
#pragma mark UICollectionViewDataSource
-(NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView {
    return 1;
}

-(NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
    return arrayDesigns.count;
}

-(UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath {
    
    static NSString *cellIdentifier = @"DesignCellIdentifier";
    
    DesignCollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:cellIdentifier forIndexPath:indexPath];
    [cell setDelegate:self];
    
    [cell addImage:arrayDesigns[indexPath.row]];
    
    return cell;
    
}

#pragma mark UICollectionViewDelegate

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath {
}


#pragma mark - DesignCollectionViewCellDelegate
- (void)didLongPressCell:(DesignCollectionViewCell *)cell withGesture:(UILongPressGestureRecognizer *)gesture {
    
    CGPoint point = [gesture locationInView:self.parentViewController.view];
    if (gesture.state == UIGestureRecognizerStateBegan) {
        NSIndexPath *indexPath = [self.collectionView indexPathForCell:cell];
        [((CanvasViewController *)self.parentViewController) addDraggableViewWithDesign:arrayDesigns[indexPath.row] atPoint:point];
    }
    else if (gesture.state == UIGestureRecognizerStateCancelled || gesture.state == UIGestureRecognizerStateFailed || gesture.state == UIGestureRecognizerStateEnded)
    {
        BOOL validDropPoint = [((CanvasViewController *)self.parentViewController) isValidDropPoint:point];
        if (!validDropPoint) {
            [((CanvasViewController *)self.parentViewController) cancelDragAndDropActivity];
        }
    }
    
}

@end
