//
//  CanvasViewController.h
//  Inspired
//
//  Created by Aswin on 24/09/16.
//  Copyright © 2016 Aswin. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CanvasViewController : UIViewController

- (void)addDraggableViewWithDesign:(UIImage *)imageSelectedDesign atPoint:(CGPoint)center;
- (BOOL)isValidDropPoint:(CGPoint)dropPoint;
- (void)cancelDragAndDropActivity;

@end
