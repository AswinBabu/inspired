//
//  PrintPreviewViewController.h
//  Inspired
//
//  Created by Aswin on 04/10/16.
//  Copyright © 2016 Aswin. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface PrintPreviewViewController : UIViewController

@property(nonatomic, strong) UIImage *imagePreview; 

@end
