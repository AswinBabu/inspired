//
//  PrintPreviewViewController.m
//  Inspired
//
//  Created by Aswin on 04/10/16.
//  Copyright © 2016 Aswin. All rights reserved.
//

#import "PrintPreviewViewController.h"

@implementation PrintPreviewViewController {
    
    __weak IBOutlet UIImageView *imageViewPreview;
}

- (void)viewDidLoad {
    [super viewDidLoad];
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    
    [imageViewPreview setImage:_imagePreview];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - ButtonAction
- (IBAction)restartButtonAction:(id)sender {
    [self.navigationController popToRootViewControllerAnimated:YES];
}

@end
